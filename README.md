# stage-2-es6-for-everyone
This is my simple realization of the legendary game "Street Fighter" as a task from binary-studio-academy.

## Functionality
- [x] Click on any fighter and it will its preview with stats in versus block
- [x] Click on fighter image in versus opens modal with change fighter stats possibility
- [x] Choose two fighter and the **_Fight_** button will be available
- [x] Click on the **_Fight_** button renders arena with two choosed fighters
- [x] Click [A] and [J] to hit, [D] and [L] to block, 
- [x] Click combinations [QWE]  or [UIO] in order to strike a critical hit
- [x] When someone's health will equal 0, **_Winner Modal_** will show fighter image
- [x] Click **_Close_** button in winner modal to complete the game

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/
