import App from './src/javascript/app';
import './src/styles/styles.css';

new App (document.getElementById('root') as HTMLDivElement, 
        document.getElementById('loading-overlay') as HTMLDivElement);