interface IControl  {
  readonly PlayerOneAttack: string;
  readonly PlayerOneBlock: string;
  readonly PlayerTwoAttack: string;
  readonly PlayerTwoBlock: string;
  readonly PlayerOneCriticalHitCombination: [string, string, string];
  readonly PlayerTwoCriticalHitCombination: [string, string, string];
}

export const controls: IControl = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
};