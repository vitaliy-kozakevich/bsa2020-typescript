import { fighterDetails } from '../helpers/mockData'
import { createElement, mockObj } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';

export function renderArena(selectedFighters: [fighterDetails, fighterDetails]) {
  const root = (document.getElementById('root') as HTMLElement);
  const arena: HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner

  fight(...selectedFighters).then((fighter) :void => showWinnerModal(<fighterDetails>fighter));
}

export function createArena(selectedFighters: [fighterDetails, fighterDetails]) {
  const arena = createElement({ tagName: 'div', className: 'arena___root', attributes: mockObj });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: fighterDetails, rightFighter: fighterDetails) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status',  attributes: mockObj });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign',  attributes: mockObj });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: fighterDetails, position: 'left' | 'right') {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator', attributes: mockObj });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name', attributes: mockObj });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator', attributes: mockObj });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: fighterDetails, secondFighter: fighterDetails) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield`, attributes: mockObj });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: fighterDetails, position: 'left' | 'right') {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
    attributes: mockObj
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
