import { fighterDetails } from '../../helpers/mockData'
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter: fighterDetails) {
  // call showModal function
  const bodyElement = createElement({ tagName: 'div', className: 'arena-modal___over', attributes: { default: 'default'} }) as HTMLDivElement;
  const fighterImage = createFighterImage(fighter);
  bodyElement.append(fighterImage);
  showModal({
    title: `${fighter.name} Wins`,
    bodyElement,
    onClose: () => {
      document.location.reload(true);
    },
  });
}
