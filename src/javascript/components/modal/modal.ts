import { createElement } from '../../helpers/domHelper';

export function showModal(el: { title: string, bodyElement: HTMLElement, onClose: () => void, changeStatsBtn?: HTMLElement }) {
  const { title, bodyElement, onClose } = el; 
  const changeStatsBtn = arguments[0].changeStatsBtn as HTMLElement;
  if (changeStatsBtn) {
    changeStatsBtn.addEventListener('click', hideModal);
  }
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(el: { title:string, bodyElement: HTMLElement, onClose: () => void}) {
  const { title, bodyElement, onClose } = el;
  const layer = createElement({ tagName: 'div', className: 'modal-layer', attributes: { default: 'default'} })  as HTMLDivElement;
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root', attributes: { default: 'default'} }) as HTMLDivElement;
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLDivElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header', attributes: { default: 'default'} }) as HTMLDivElement;
  const titleElement = createElement({ tagName: 'span', attributes: { default: 'default'} }) as HTMLElement;
  const closeButton = createElement({ tagName: 'div', className: 'close-btn', attributes: { default: 'default'} }) as HTMLElement;

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    setTimeout(() => {
      onClose();
    }, 2000);
    hideModal();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
