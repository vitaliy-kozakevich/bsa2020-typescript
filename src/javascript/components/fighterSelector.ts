import { fighterDetails, fighterSetup } from '../helpers/mockData'
import { createElement, mockObj } from '../helpers/domHelper';
import { renderArena } from './arena';
// import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';

export function createFightersSelector() {
  let selectedFighters: [fighterDetails, fighterDetails];

  return async (event: Event, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

export const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string) {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (!fighterDetailsMap.has(fighterId)) {
    const fighter = await fighterService.getFighterDetails(fighterId);
    const { _id } = fighter;
    fighterDetailsMap.set(_id, fighter);
  }
  const fighter = fighterDetailsMap.get(fighterId);
  return fighter;
}

function changeFighterStats(selectedFighters: [fighterDetails, fighterDetails]) {
  return (fighter: fighterDetails, inputs: HTMLCollection) => {
    const { _id } = fighter;
    selectedFighters.map((fighter) => {
      if (fighter && inputs && fighter._id === _id) {
        fighter.health = +inputs[0].TEXT_NODE;
        fighter.attack = +inputs[1].TEXT_NODE;
        fighter.defense = +inputs[2].TEXT_NODE;
      }
      return fighter;
    });
    return renderSelectedFighters(selectedFighters);
  };
}

function renderSelectedFighters(selectedFighters: [fighterDetails, fighterDetails]) {
  const fightersPreview  = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters.map((fighter) => {
    if (fighter) {
      fighter.changeStats = changeFighterStats(selectedFighters);
    }
    return fighter;
  });
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: [fighterDetails, fighterDetails]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block', attributes: mockObj });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: 'mock' },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
    attributes: mockObj
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: [fighterDetails, fighterDetails]) {
  renderArena(selectedFighters);
}
