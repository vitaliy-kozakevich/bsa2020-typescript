import { controls } from '../../constants/controls';
import { createElement, mockObj } from '../helpers/domHelper';
import { fighterDetails } from '../helpers/mockData'

type GettingNumFunc<N> = (num1: N, num2: N) => N

type ResolveFunc<R> = (value: R) => R

function getRandomNumber(min: number, max: number) :number {
  return min + Math.random() * (max - min);
}

export function getDamage(attacker: number, defender: number) :number {
  // return damage
  return defender >= attacker ? 0 : attacker - defender;
}

export function getHitPower(attack: number) :number {
  // return hit power
  const criticalHitChance = getRandomNumber(1, 2);
  return criticalHitChance * attack;
}

export function getBlockPower(defense: number) :number {
  // return block power
  const dodgeChance = getRandomNumber(1, 2);
  return dodgeChance * defense;
}

export function getCriticalHit(attack: number) :number {
  // return critical hit power
  const criticalHit = attack * 2;
  return criticalHit;
}

function addCriticalTextBlock() :void {
  const critical = createElement({ tagName: 'div', className: 'arena___critical-block', attributes: mockObj });
  const arenaRoot = document.querySelector('.arena___root') as HTMLElement
  critical.innerText = 'CRITICAL'; 
  arenaRoot.append(critical);
  setTimeout(() => {
    critical?.remove();
  }, 2100);
}

function addPunchBackground(element: HTMLElement, className: string) :void {
  setTimeout(() => {
    element.classList.toggle(className);
  }, 1200);
  setTimeout(() => {
    element.classList.toggle(className);
  }, 2100);
}

function animateAttack(element: HTMLElement, className: string) :void {
  element.classList.toggle(className);
  setTimeout(() => {
    element.classList.toggle(className);
  }, 2100);
}

function setHealth(fighter: fighterDetails, damage: number, el: HTMLElement, value: number) {
  fighter.health = fighter.health - damage;
  el.style.width = `${value}%`;
}

function getHealthValue(damageValue: number, healthValue: number) :number {
  healthValue = healthValue - (damageValue / healthValue) * 100;
  return healthValue;
}

// resolve winner
function resolveWinner(resolve: (value: fighterDetails | PromiseLike<fighterDetails>) => void, firstFighter: fighterDetails, secondFighter: fighterDetails) {
  if (firstFighter.health <= 0) {
    resolve(secondFighter);
  } else if (secondFighter.health <= 0) {
    resolve(firstFighter);
  }
}

export async function fight(firstFighter: fighterDetails, secondFighter: fighterDetails) {
  return new Promise<fighterDetails>((resolve)  => {
    const pressedKeys = new Set();
    const leftFighter = document.querySelector('.arena___left-fighter');
    const rightFighter = document.querySelector('.arena___right-fighter');
    const leftHealth = document.getElementById('left-fighter-indicator');
    const rightHealth = document.getElementById('right-fighter-indicator');
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;
    let damageToLeftFighter = 0;
    let damageToRightFighter = 0;
    let leftHealthValue = 100;
    let rightHealthValue = 100;
    let interval = 10;

    function hit(firstFighter: fighterDetails, secondFighter: fighterDetails) {
      if (pressedKeys.has(PlayerOneAttack) && !pressedKeys.has(PlayerOneBlock)) {
        const damage = getDamage(getHitPower(firstFighter.attack), getBlockPower(secondFighter.defense));
        const healthValue = getHealthValue(damage, rightHealthValue);
        damageToRightFighter = damage;
        rightHealthValue = healthValue;
        animateAttack(leftFighter as HTMLElement, 'animate');
      } else if (pressedKeys.has(PlayerTwoAttack) && !pressedKeys.has(PlayerTwoBlock)) {
        const damage = getDamage(getHitPower(secondFighter.attack), getBlockPower(firstFighter.defense));
        const healthValue = getHealthValue(damage, leftHealthValue);
        damageToLeftFighter = damage;
        leftHealthValue = healthValue;
        animateAttack(rightFighter as HTMLElement, 'animate-right');
      }
    }

    function controlHealth(firstFighter: fighterDetails, secondFighter: fighterDetails) {
      if (pressedKeys.has(PlayerTwoAttack) && !pressedKeys.has(PlayerOneBlock)) {
        setHealth(firstFighter, damageToLeftFighter, leftHealth as HTMLElement, leftHealthValue);
        addPunchBackground(leftFighter as HTMLElement, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      } else if (pressedKeys.has(PlayerOneAttack) && !pressedKeys.has(PlayerTwoBlock)) {
        setHealth(secondFighter, damageToRightFighter, rightHealth as HTMLElement, rightHealthValue);
        addPunchBackground(rightFighter as HTMLElement, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      }
    }

    function criticalHit(firstFighter: fighterDetails, secondFighter: fighterDetails, code: string) {
      if (PlayerOneCriticalHitCombination.includes(code)) {
        const damage = getCriticalHit(firstFighter.attack);
        const healthValue= getHealthValue(
          damage,
          rightHealthValue
        );
        damageToRightFighter = damage;
        rightHealthValue = healthValue;
        setHealth(secondFighter, damageToRightFighter, rightHealth as HTMLElement, rightHealthValue);
        animateAttack(leftFighter as HTMLElement, 'animate');
        addCriticalTextBlock();
        addPunchBackground(rightFighter as HTMLElement, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      } else if (PlayerTwoCriticalHitCombination.includes(code)) {
        const damage = getCriticalHit(secondFighter.attack);
        const healthValue = getHealthValue(
          damage,
          leftHealthValue
        );
        damageToLeftFighter = damage;
        leftHealthValue = healthValue;
        setHealth(firstFighter, damageToLeftFighter, leftHealth as HTMLElement, leftHealthValue);
        animateAttack(rightFighter as HTMLElement, 'animate-right');
        addCriticalTextBlock();
        addPunchBackground(leftFighter as HTMLElement, 'punch');
        resolveWinner(resolve, firstFighter, secondFighter);
      }
    }

    function handlePressedKeysEvent(e: KeyboardEvent) {
      pressedKeys.add(e.code);
    }

    function handleUnpressedKeysEvent(e: KeyboardEvent) {
      hit(firstFighter, secondFighter);
      controlHealth(firstFighter, secondFighter);
      if (interval === 10 && pressedKeys.size === 3) {
        criticalHit(firstFighter, secondFighter, e.code);
        let timer = setInterval(() => {
          interval--;
          if (interval === 0) {
            clearInterval(timer);
            interval = 10;
          }
        }, 1000);
      }
      pressedKeys.delete(e.code);
    }

    document.addEventListener('keydown', handlePressedKeysEvent);
    document.addEventListener('keyup', handleUnpressedKeysEvent);
  });
}