import { fighterSetup } from '../helpers/mockData'
import { createElement, mockObj } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters: fighterSetup[]) {
  const selectFighter: (event: Event, fighterId: string) => Promise<void> = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root', attributes: mockObj });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root', attributes: mockObj });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list', attributes: mockObj });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

export function createFighter(fighter: fighterSetup, selectFighter:  (event: Event, fighterId: string) => Promise<void>) {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter',  attributes: mockObj });
  const imageElement = createImage(fighter);
  const onClick = async (event: Event) => await selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: fighterSetup) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}