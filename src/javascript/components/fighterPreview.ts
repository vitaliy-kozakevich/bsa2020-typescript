import { fighterDetails, fighterSetup } from '../helpers/mockData'
import { createElement, mockObj } from '../helpers/domHelper';
import { createFighter } from './fightersView';
import { showModal } from './modal/modal';
import { fighterDetailsMap } from './fighterSelector';

export function createFighterPreview(fighter: fighterDetails, position: 'left' | 'right') {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    attributes: mockObj
  });
  const fighterInfo = createElement({ tagName: 'div', className: `fighter-preview__info`, attributes: mockObj });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { _id, source, changeStats, ...rest } = fighter;
    const stats = Object.keys(rest).reduce((acc, curr) => {
      const stat: string = `${curr}: ${Object(fighter)[curr]}`;
      const span = createElement({ tagName: 'span', className: 'fighter-preview-info___stats', attributes: mockObj });
      span.innerText = stat;
      acc.append(span);
      return acc;
    }, fighterInfo);
    const imageElement = createFighter(fighter, clickFighterPreviewImg());
    fighterElement.append(imageElement, stats);
  }
  return fighterElement;
}

function clickFighterPreviewImg() {
  return async (event: Event, fighterId: string) => {
    const fighter = fighterDetailsMap.get(fighterId);
    const { name: title, changeStats, source, _id, ...rest } = fighter;
    const bodyElement: HTMLElement = createElement({ tagName: 'div', className: 'fighter-preview___modal', attributes: mockObj });
    const changeStatsBtn = createElement({ tagName: 'button', className: 'fighter-preview-modal___stats-btn', attributes: mockObj });
    const newStats = Object.keys(rest).map((el) =>
      createElement({ tagName: 'input', className: 'preview-modal-stats___input', attributes: mockObj })
    );
    const defaultStats = Object.keys(rest).map((el) => {
      el = `${el}: ${fighter[el]}`;
      const span = createElement({ tagName: 'span', className: 'preview-modal-stats___span', attributes: mockObj });
      span.innerText = el;
      return span;
    });
    const modalStatsElements = Object.keys(rest).map((el: string | HTMLElement, index: number) => {
      switch (index) {
        case 0:
          el = createElement({ tagName: 'div', className: 'preview-modal___stats', attributes: mockObj });
          el.append(...defaultStats);
          break;
        case 1:
          el = createElement({ tagName: 'div', className: 'preview-modal___stats modal-stats___inputs', attributes: mockObj });
          el.append(...newStats);
          break;
        case 2:
          el = createElement({ tagName: 'div', className: 'preview-modal___stats', attributes: mockObj });
          el.append(changeStatsBtn);
          break;
        default:
          break;
      }
      return el;
    });
    changeStatsBtn.innerText = 'Change fighter stats';
    
    function handleClick() {
      const inputs = document.getElementsByClassName('preview-modal-stats___input');
      changeStats(fighter, inputs);
    }

    changeStatsBtn.addEventListener('click', (e) => {
      handleClick();
    });

    bodyElement.append(...modalStatsElements);
    showModal({
      title,
      bodyElement,
      onClose: () => {},
      changeStatsBtn,
    });
  };
}

export function createFighterImage(fighter: fighterSetup) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
