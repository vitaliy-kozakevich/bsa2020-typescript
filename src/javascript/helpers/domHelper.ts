interface IAttrib {
  [index: string]: string;
}

export const mockObj = { data: 'null'}

export function createElement(el: { tagName: string, className?: string, attributes: IAttrib } ): HTMLElement {
  const { tagName, className, attributes } = el;
  const element  = document.createElement(tagName) as HTMLElement;

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
