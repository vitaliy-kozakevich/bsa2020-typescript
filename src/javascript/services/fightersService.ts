import { fighterSetup, fighterDetails } from '../helpers/mockData'
import { callApi } from '../helpers/apiHelper';


class FighterService {
  async getFighters(): Promise<fighterSetup[] | Error>  {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return <fighterSetup[] | Error>apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<fighterDetails>  {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as fighterDetails;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService: FighterService = new FighterService();
