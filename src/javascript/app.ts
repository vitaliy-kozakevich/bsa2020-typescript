import { fighterSetup } from './helpers/mockData'
import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

interface IApp {
  startApp(): void;
}

class App<T extends HTMLElement> implements IApp {
  constructor(private rootElement: T, private loadingElement: T) {
    this.startApp();
  }
  
  public async startApp(): Promise<void> {
    try {
      this.loadingElement.style.visibility  = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters as fighterSetup[]);

      this.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      this.rootElement.innerText = 'Failed to load data';
    } finally {
      this.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
